declare module "geotiff" {
  type TypedArray =
    | Uint8Array
    | Int8Array
    | Uint16Array
    | Int16Array
    | Uint32Array
    | Int32Array
    | Float32Array
    | Float64Array;

  type TransformMatrix = [number, number, number, number, number, number];

  function fromUrl(
    url: string,
    headers?: Record<string, unknown>
  ): Promise<GeoTIFF>;
  function fromBlob(blob: Blob): Promise<GeoTIFF>;

  class GeoTIFF {
    cache: unknown;
    dataView: DataView;
    getImage(index?: number): Promise<GeoTIFFImage>;
    ifdRequests: { [key: number]: Promise<ImageFileDirectory> };
    littleEndian: boolean;
    parseFileDirectoryAt(offset: number): Promise<ImageFileDirectory>;
    readRasters(options?: RasterOptions): Promise<SupportedTypedArray>;
    source: unknown;
  }

  interface Pool {
    decode(
      fileDirectory: FileDirectory,
      buffer: ArrayBuffer
    ): Promise<ArrayBuffer>;
  }

  interface RasterOptions {
    bbox?: number[];
    enableAlpha?: boolean;
    fillValue?: number | number[];
    height?: number;
    interleave?: boolean;
    pool?: Pool;
    resampleMethod?: string;
    samples?: number[];
    signal?: AbortSignal;
    width?: number;
    window?: Extent;
  }

  interface FileDirectory {
    ImageDescription: string;
    SubIFDs?: number[];
    ModelPixelScale: number[];
    PhotometricInterpretation?: number;
  }

  interface ImageFileDirectory {
    fileDirectory: FileDirectory;
    geoKeyDirectory: unknown;
  }

  type RasterData = {
    height: number;
    maxs: [number];
    mins: [number];
    noDataValue: unknown;
    numberOfRasters: number;
    pixelHeight: number;
    pixelWidth: number;
    projection: number;
    ranges: [number];
    rasterType: string;
    sourceType: string;
    values: Array<TypedArray>;
    width: number;
    xmax: number;
    xmin: number;
    ymax: number;
    ymin: number;
    _blob_is_available: boolean;
    _data: ArrayBuffer;
    _url_is_available: boolean;
    _web_worker_is_available: boolean;
  };

  // type RasterData = Array<TypedArray> & {
  //   width: number;
  //   height: number;
  // };

  // GDAL style extent: xmin ymin xmax ymax
  type Extent = [number, number, number, number];

  class GeoTIFFImage {
    constructor(
      cache: unknown,
      dataView: DataView,
      fileDirectory: FileDirectory,
      geoKeyDirectory: unknown,
      littleEndian: boolean,
      source: unknown
    );
    fileDirectory: FileDirectory;
    getBoundingBox: () => Extent;
    getBytesPerPixel: () => number;
    getFileDirectory: () => FileDirectory;
    getHeight: () => number;
    getOrigin: () => [number, number, number];
    getResolution: () => [number, number, number];
    getSamplesPerPixel: () => number;
    getTiePoints: () => {
      i: number;
      j: number;
      k: number;
      x: number;
      y: number;
      z: number;
    }[];
    getTileHeight: () => number;
    getTileWidth: () => number;
    getWidth: () => number;
    pixelIsArea: () => boolean;
    readRasters(options?: RasterOptions): Promise<RasterData>;
  }
}

// Imported in src/loaders/tiff/Pool/decoder.worker.js
declare module "geotiff/src/compression" {
  import type { FileDirectory } from "geotiff";

  interface Decoder {
    decode(
      fileDirectory: FileDirectory,
      buffer: ArrayBuffer
    ): Promise<ArrayBuffer>;
  }
  export function getDecoder(fileDirectory: FileDirectory): Decoder;
}

declare module "georaster";
declare module "georaster-layer-for-leaflet";
declare module "geoblaze";
