# Keshtyaar - Lead Front-End Challenge

This is a challenge for Keshtyaar interview.

## Getting started

- Clone the project with: `git clone git@gitlab.com:cinna/keshtyaar-challenge.git`
- Move to the project directory
- Run the server with: `yarn dev`

## Development

### Environment variables

The `.env.example` file contains all our env variables.
To run the project locally, you have to create a copy of it changing the name in `.env.local`.

`cp .env.example .env.local`

and change the values using our information.

### Create a component

- Each component must be added under the `components`, `pages` or `layout` directory.
- Create the directory with the name of the component. (e.g. `button`)
- The `index.tsx` file contains the component.
- The `types.d.ts` file contains the component types and interfaces.

### Naming guidelines

| modules                                                                             | name convention                                      |
| ----------------------------------------------------------------------------------- | ---------------------------------------------------- |
| _dir names_                                                                         | `kebab-case`                                         |
| _file names_, _variables_, _methods_, _functions_, _function parameters_, _fields_, | `camelCase`                                          |
| _components_\*, _pages_                                                             | `PascalCase`                                         |
| _interfaces_, _types_, _classes_                                                    | `PascalCase` (class names)<br/>`camelCase` (members) |
| _global variables_, _constants_                                                     | `Upper_Case_Snake_Case`                              |

### How to commit

Here there are some simple rules to write documental commit messages:

```
# The subject line should contain 50 characters along with the issue id and the hashtag symbol:
# {#ISSUE_ID} - {50-character subject}
#
# 72-character wrapped longer description. This should answer:
#
# * Why was this change necessary?
# * How does it address the problem?
# * Are there any side effects?
#
# Include a link to the ticket, if any.
```

## Notes

This example shows how to integrate the TypeScript type system into Next.js. Since TypeScript is supported out of the box with Next.js, all we have to do is to install TypeScript.

```
npm install --save-dev typescript
```

To enable TypeScript's features, we install the type declarations for React and Node.

```
npm install --save-dev @types/react @types/react-dom @types/node
```

When we run `next dev` the next time, Next.js will start looking for any `.ts` or `.tsx` files in our project and builds it. It even automatically creates a `tsconfig.json` file for our project with the recommended settings.

Next.js has built-in TypeScript declarations, so we'll get autocompletion for Next.js' modules straight away.

A `type-check` script is also added to `package.json`, which runs TypeScript's `tsc` CLI in `noEmit` mode to run type-checking separately. You can then include this, for example, in your `test` scripts.
