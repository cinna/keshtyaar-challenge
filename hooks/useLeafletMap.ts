import { useRef } from "react";
import { Map as LeafletMap } from "leaflet";

import { INITIAL_POSITION, INITIAL_ZOOM } from "../constants";

const useLeafletMap = () => {
  const firstMapRef = useRef<LeafletMap>();
  const secondMapRef = useRef<LeafletMap>();

  const sync = (map1: LeafletMap, map2: LeafletMap) => {
    map1?.setView(map2.getCenter(), map2.getZoom(), {
      animate: false,
      duration: 1,
    });
  };

  return {
    INITIAL_POSITION,
    INITIAL_ZOOM,
    sync,
    mapRefs: [firstMapRef, secondMapRef],
  };
};

export default useLeafletMap;
