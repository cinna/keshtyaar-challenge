import { NextPage } from "next";
import dynamic from "next/dynamic";

import { DefaultLayout } from "../../layout";

const MapBox = dynamic(import("../../components/map-box"), {
  ssr: false,
  // eslint-disable-next-line react/display-name
  loading: () => (
    <div style={{ textAlign: "center", paddingTop: 20 }}>loading…</div>
  ),
});

const MapPage: NextPage = () => {
  return (
    <DefaultLayout>
      <MapBox />
    </DefaultLayout>
  );
};

export default MapPage;
