import { NextPage, NextPageContext } from "next";
import { AppProps } from "next/app";
import Head from "next/head";

import { APP_TITLE } from "../constants";

import "../styles/index.css";

const App: NextPage<AppProps> = ({ Component, pageProps }) => (
  <>
    <Head>
      <title>{APP_TITLE}</title>
      <meta charSet="utf-8" />
      <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      <meta name="description" content="Keshtyaar - Lead Front-End Challenge" />
      <link rel="shortcut icon" href="/images/favicon.png" />
      <link rel="apple-touch-icon" href="/images/favicon.png" />
      <link rel="manifest" href="/manifest.json" />
    </Head>
    <Component {...pageProps} />
  </>
);

export const getInitialProps = async ({
  Component,
  ctx,
}: {
  Component: NextPage;
  ctx: NextPageContext;
}): Promise<unknown> => {
  let pageProps = {};

  if (Component.getInitialProps) {
    pageProps = await Component.getInitialProps(ctx);
  }

  return { pageProps };
};

export default App;
