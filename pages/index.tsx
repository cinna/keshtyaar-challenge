import { NextPage } from "next";
import Link from "next/link";

import { DefaultLayout } from "../layout";

const IndexPage: NextPage = () => (
  <DefaultLayout>
    <h2>Hello World! 👋</h2>
    <nav>
      <Link href="/map">maps</Link>
    </nav>
  </DefaultLayout>
);

export default IndexPage;
