import chroma from "chroma-js";

import type { Extent } from "geotiff";

export const hasDataForAllBands = (values: Extent): boolean =>
  values.every((value) => !!value || !isNaN(value));

export const setPixelColours = (values: Extent, min = 0, max = 1): string => {
  // Check if the pixel should be coloured, if not make it transparent
  if (!hasDataForAllBands(values)) {
    return "";
  }

  const [red, green, blue, nir = 1] = values;

  return chroma.scale("Spectral").domain([min, max])(red).hex();
};
