import * as GeoTIFF from "geotiff";

import type { GeoTIFFImage, TransformMatrix } from "geotiff";

export const loadGeoTiff = async (
  data: Blob
): Promise<{
  image: GeoTIFF.GeoTIFFImage;
  rasters: GeoTIFF.RasterData;
  transform: GeoTIFF.TransformMatrix;
}> => {
  const raw = await GeoTIFF.fromBlob(data);
  const image = await raw.getImage();
  const rasters = await image.readRasters();
  const transform = getTransform(image);
  return {
    image,
    rasters,
    transform,
  };
};

export const getTransform = (geoTiffImage: GeoTIFFImage): TransformMatrix => {
  const tiepoint = geoTiffImage.getTiePoints()[0];
  const pixelScale = geoTiffImage.getFileDirectory().ModelPixelScale;
  return [
    tiepoint.x,
    pixelScale[0],
    0,
    tiepoint.y,
    0,
    -1 * pixelScale[1],
  ] as TransformMatrix;
};
