import { FC } from "react";
import Head from "next/head";

import { APP_TITLE } from "../../constants";

import type { Props } from "./types.d";

const DefaultLayout: FC<Props> = ({ title = APP_TITLE, children }) => (
  <div>
    <Head>
      <title>{title}</title>
    </Head>
    <header>
      <h1>{APP_TITLE}</h1>
    </header>
    {children}
    <footer>
      <hr />
      <span>
        View this project in{" "}
        <a
          href="https://gitlab.com/cinna/keshtyaar-challenge"
          target="_blank"
          rel="noopener noreferrer"
        >
          Gitlab
        </a>
      </span>
    </footer>
  </div>
);

export default DefaultLayout;
