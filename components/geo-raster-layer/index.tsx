import { FC, useEffect, useState } from "react";
import { useMap, Popup } from "react-leaflet";
import GeoRasterLayerForLeaflet from "georaster-layer-for-leaflet";
import parseGeoraster from "georaster";
import { LatLngExpression, Layer } from "leaflet";
import geoblaze from "geoblaze";

import { setPixelColours } from "../../utils";

import type { Extent, RasterData } from "geotiff";
import type { Props } from "./types.d";

const GeoRasterLayer: FC<Props> = ({
  selectedFile,
  onProcessEnd,
  onSelect,
  coordinates,
  id,
  ...options
}) => {
  const map = useMap();

  const [rasters, setRasters] = useState();
  const [tiffLayer, setTiffLayer] = useState<Layer>();
  const [popup, setPopup] = useState<{
    id: number;
    position: LatLngExpression;
    content: string;
  }>();

  useEffect(() => {
    (() => {
      const reader = new FileReader();
      reader.readAsArrayBuffer(selectedFile);
      reader.onloadend = async () => {
        const arrayBuffer = reader.result;
        const georaster = await parseGeoraster(arrayBuffer);
        onProcessEnd(id, georaster as RasterData);
        const layer = new GeoRasterLayerForLeaflet({
          ...options,
          georaster,
          opacity: 0.7,
          resolution: georaster.projection,
          pixelValuesToColorFn: (values: Extent) => {
            const { mins, maxs } = georaster;
            return setPixelColours(values, mins[0], maxs[0]);
          },
        });
        setRasters(georaster);
        setTiffLayer(layer);
        if (tiffLayer) {
          map.removeLayer(tiffLayer);
        }
      };
    })();
  }, [selectedFile]);

  useEffect(() => {
    if (tiffLayer) {
      map.addLayer(tiffLayer);
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      map.fitBounds((tiffLayer as any).getBounds());
    }
    return () => {
      if (tiffLayer) {
        map.removeLayer(tiffLayer);
      }
    };
  }, [tiffLayer]);

  useEffect(() => {
    if (rasters) {
      map.on("click", onSelect);
    }
    return () => {
      map.off("click");
    };
  }, [rasters]);

  useEffect(() => {
    if (coordinates) {
      try {
        const latlng = [coordinates.lng, coordinates.lat];
        const result = geoblaze.identify(rasters, latlng);
        if (result && !isNaN(result)) {
          setPopup((prev) => ({
            id: (prev?.id || 0) + 1,
            position: coordinates,
            content: Number(result).toFixed(4),
          }));
        }
      } catch (error) {
        console.log(error);
      }
    }
  }, [coordinates]);

  return popup ? (
    <Popup key={`popup-${popup.id}`} position={popup.position}>
      Result: {popup.content}
    </Popup>
  ) : (
    <></>
  );
};

export default GeoRasterLayer;
