import { RasterData } from "geotiff";

export type Props = {
  id: number;
  selectedFile: File;
  onSelect: (event: LeafletMouseEvent) => void;
  onProcessEnd: (id: number, event: RasterData) => void;

  coordinates?: LeafletMouseEvent.latlng;
} & Omit<GeoRasterLayerOptions, "georaster" | "georasters">;
