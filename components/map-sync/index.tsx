import { FC, useEffect } from "react";
import { useMapEvents } from "react-leaflet";

import { useLeafletMap } from "../../hooks";

import type { MapSyncProps } from "./types.d";

const MapSync: FC<MapSyncProps> = ({ otherMap }) => {
  const { sync } = useLeafletMap();

  const map = useMapEvents({
    drag: ({ target }) => otherMap.current && sync(otherMap.current, target),
    zoomend: ({ target }) => otherMap.current && sync(otherMap.current, target),
  });

  useEffect(() => {
    if (otherMap.current) {
      sync(otherMap.current, map);
    }
  }, [map, otherMap]);

  return <></>;
};

export default MapSync;
