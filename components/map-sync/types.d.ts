import { MutableRefObject } from "react";
import { Map as LeafletMap } from "leaflet";

export type MapSyncProps = {
  otherMap: MutableRefObject<LeafletMap | undefined>;
};
