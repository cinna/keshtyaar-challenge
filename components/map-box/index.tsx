import { FC, useState, ChangeEvent, useEffect } from "react";
import { LatLngExpression, LeafletMouseEvent } from "leaflet";
import { RasterData } from "geotiff";

import { Map } from "..";
import { useLeafletMap } from "../../hooks";

import type { ViewportType } from "./types.d";

const MapBox: FC = () => {
  const { INITIAL_POSITION, INITIAL_ZOOM, mapRefs } = useLeafletMap();

  const [viewport, setViewport] = useState<ViewportType>({
    center: INITIAL_POSITION,
    zoom: INITIAL_ZOOM,
  });
  const [selectedFile, setSelectedFile] = useState<FileList>();

  const handleChange = async (
    event: ChangeEvent<HTMLInputElement>
  ): Promise<void> => {
    try {
      if (event.target.files?.length) {
        setSelectedFile(event.target.files);
      } else {
        throw Error("No file has selected.");
      }
    } catch (error) {
      console.error(error);
    }
  };

  const [rasterArr, setRasterArr] = useState<RasterData[]>([]);
  const [coordinates, setCoordinates] = useState<LatLngExpression>();

  const handleProcessEnd = (id: number, rasterData: RasterData) => {
    setRasterArr((prev) =>
      id ? [prev[0], rasterData] : [rasterData, prev[1]]
    );
  };

  const handleSelect = (event: LeafletMouseEvent) => {
    setCoordinates(event.latlng);
  };

  const [similars, setSimilars] = useState<number>();

  useEffect(() => {
    if (rasterArr) {
      const arr1 = new Int32Array(rasterArr[0]?._data);
      const arr2 = new Int32Array(rasterArr[1]?._data);
      setSimilars(arr1?.filter((item, i) => item === arr2[i]).length);
    }
  }, [rasterArr]);

  return (
    <>
      <input
        type="file"
        multiple
        min={2}
        max={2}
        accept="image/tiff"
        onChange={handleChange}
      />
      <div className="map-container">
        {[0, 1].map((id) => (
          <Map
            id={id}
            isComparing
            mapRefs={mapRefs}
            key={`leaf-map${id}`}
            className="leaflet-map"
            center={viewport.center}
            zoom={viewport.zoom}
            selectedFile={selectedFile?.[id]}
            whenCreated={(map) => {
              mapRefs[id].current = map;
            }}
            onSelect={handleSelect}
            onProcessEnd={handleProcessEnd}
            coordinates={coordinates}
          />
        ))}
      </div>
      {rasterArr.length > 0 && <h3>Number of similar points: {similars}</h3>}
    </>
  );
};

export default MapBox;
