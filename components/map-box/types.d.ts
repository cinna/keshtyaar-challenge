export type ViewportType = {
  center: [number, number];
  zoom: number;
};
