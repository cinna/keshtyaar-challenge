import { FC } from "react";
import { MapContainer, TileLayer } from "react-leaflet";

import { GeoRasterLayer, MapSync } from "..";
import { URL, ATTRIBUTION } from "../../constants";

import type { Props } from "./types.d";

const Map: FC<Props> = ({
  id,
  mapRefs,
  selectedFile,
  isComparing = false,
  coordinates,
  onProcessEnd,
  onSelect,
  ...options
}) => {
  return (
    <MapContainer {...options}>
      <TileLayer url={URL} attribution={ATTRIBUTION} />
      {selectedFile && (
        <GeoRasterLayer
          id={id}
          selectedFile={selectedFile}
          onProcessEnd={onProcessEnd}
          onSelect={onSelect}
          coordinates={coordinates}
        />
      )}
      {isComparing && <MapSync otherMap={mapRefs[Number(!id)]} />}
    </MapContainer>
  );
};

export default Map;
