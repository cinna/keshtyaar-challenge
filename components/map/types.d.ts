import { MutableRefObject } from "react";

import type { MapContainerProps } from "react-leaflet";
import { Map as LeafletMap } from "leaflet";

export interface Props extends MapContainerProps {
  id: number;
  mapRefs: Array<MutableRefObject<LeafletMap | undefined>>;
  onSelect: (event: LeafletMouseEvent) => void;
  onProcessEnd: (id: number, event: RasterData) => void;

  selectedFile?: File;
  isComparing?: boolean;
  coordinates?: LeafletMouseEvent.latlng;
}
