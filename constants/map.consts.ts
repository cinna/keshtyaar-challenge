export const URL = "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png";
export const ATTRIBUTION =
  '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors';

export const INITIAL_POSITION: [number, number] = [
  32.41959479711854, 53.6031885147094729,
];
export const INITIAL_ZOOM = 3;
