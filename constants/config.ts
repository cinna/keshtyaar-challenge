// server environment variables
export const API_TIMEOUT = Number(process.env.API_TIMEOUT || 30000);
export const DEFAULT_PORT = Number(process.env.DEFAULT_PORT || 3000);
export const DEFAULT_LOCALE: string = process.env.DEFAULT_LOCALE || "en_US";
export const DOMAIN: string = process.env.DOMAIN || "https://keshtyaar.ir/";
export const SITE_URL: string =
  process.env.SITE_URL || "https://keshtyaar-challenge.vercel.app/";

// browser environment variables
export const APP_TITLE: string =
  process.env.NEXT_PUBLIC_APP_TITLE || "Keshtyaar - Lead Front-End Challenge";

// utility constants
export const isBrowser = typeof window !== "undefined";
export const isProduction = process.env.NODE_ENV === "production";
